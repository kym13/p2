> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Kristopher Mangahas

### Project 1 # Requirements

*Deliverables:*

1. Provide Bitbucket read-only access to p2 repo, *must* include README.md, using Markdown syntax
2. Blackboard Links: p2 Bitbucket repo
4. Screenshot : Success of Delete
5. Screenshot : Success of Update




#### Assignment Screenshots:


*Screenshot of Customer Table Before Delete:

![Customer Table Screenshot](img/bdelete.png)


*Screenshot of a Confirmation of Deletion*:

![Passed validation Screenshot](img/sure.png)



*Screenshot of Customer Table After Deletion*:

![Customer Table Screnshot](img/adelete.png)



*Screenshot of Customer Table Before Update:

![Customer Table Screenshot](img/bupdate.png)


*Screenshot of a Update Procedure*:

![Passed validation Screenshot](img/update.png)



*Screenshot of Customer Table After Update*:

![Customer Table Screnshot](img/aupdate.png)